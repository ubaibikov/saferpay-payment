<?php


namespace Tygh\Payments\Processors;

use Tygh\Http;

class Saferpay
{
    protected $_url = '';
    protected $_response;
    protected $_currency;
    protected $_terminal;
    protected $_customer_id;
    protected $_payment_methods;
    protected $_wallets;
    protected $_username;
    protected $_password;
    protected $_lang;

    protected $_error_name;
    protected $_error_message;

    public function __construct($processor_data)
    {
        $this->_username = $processor_data['processor_params']['username'];
        $this->_password = $processor_data['processor_params']['password'];
        $this->_terminal = $processor_data['processor_params']['terminal'];
        $this->_customer_id = $processor_data['processor_params']['customer_id'];
        $this->_currency = $processor_data['processor_params']['currency'];
        $this->_payment_methods = $processor_data['processor_params']['payment_methods'];
        $this->_wallets = $processor_data['processor_params']['wallets'];
        $this->_lang =  $processor_data['processor_params']['lang'];
        if ($processor_data['processor_params']['mode'] == 'test') {
            $this->_url = "https://test.saferpay.com/api/Payment/v1/";
        } else {
            $this->_url = "https://www.saferpay.com/api/Payment/v1/";
        }
    }

    /**
     * @param array $order_info Is order information
     * @param string $protocol  Is a protocol for fn-url function
     * @return $this->_response Is response Saferpay payment
     */
    public function saferpayPaymentInitialize($order_info, $protocol = 'current')
    {
        $order_id = $order_info['order_id'];

        $payload = array(
            'RequestHeader' => array(
                'SpecVersion' => "1.16",
                'CustomerId' => $this->_customer_id,
                'RequestId' => (string) $order_id,
                'RetryIndicator' => 0,
            ),
            'TerminalId' => $this->_terminal,
            'PaymentMethods' => $this->_payment_methods,
            'Payment' => array(
                'Amount' => array(
                    'Value' => $this->getCurentSaferpayValue($order_info['total']),
                    'CurrencyCode' => $this->_currency,
                ),
                'OrderId' => (string) $order_id,
                'Description' => "order is $order_id",
            ),
            'PaymentMethodsOptions' => array(
                'Wallets' => $this->_wallets,
            ),
            'Payer' => array(
                'LanguageCode' => !empty($this->_lang) ? $this->_lang : CART_LANGUAGE,
                'FirstName' =>  $order_info['firstname'],
                'LastName' => $order_info['lastname'],
            ),
            'ReturnUrls' => array(
                'Success' => fn_url("payment_notification.return?payment=saferpay&ordernumber=$order_id", AREA, $protocol),
                'Fail' => fn_url("payment_notification.error?payment=saferpay&ordernumber=$order_id", AREA, $protocol),
                'Abort' => fn_url("payment_notification.abort?payment=saferpay&ordernumber=$order_id", AREA, $protocol),
            ),
        );

        $saferpay_response = $this->_response;

        $saferpay_response = Http::post($this->_url . 'PaymentPage/Initialize', json_encode($payload), $this->getSaferpayHeaders());
        $saferpay_response = json_decode($saferpay_response, true);

        if (!empty($saferpay_response['ErrorMessage'])) {
            $this->_error_name = $saferpay_response['ErrorName'];
            $this->_error_message = $saferpay_response['ErrorMessage'];
        } else {
            $token['payment_info']['token'] = $saferpay_response['Token'];
            fn_update_order_data($order_id, $token);
        }

        $saferpay_response['order_id'] = $order_info['order_id'];

        return $saferpay_response;
    }

    public function saferpayPaymentAssert(string $request_id, string $token)
    {
        $saferpay_assert = [
            "RequestHeader" => [
                "SpecVersion" => "1.16",
                "CustomerId" => $this->_customer_id,
                "RequestId" => $request_id,
                "RetryIndicator" => 0
            ],
            "Token" => $token
        ];

        $assert_response = Http::post($this->_url . 'PaymentPage/Assert', json_encode($saferpay_assert), $this->getSaferpayHeaders());
        $assert_response = json_decode($assert_response, true);

        return $assert_response;
    }

    public function saferpayPaymentCapture(string $request_id, string $transaction_id)
    {
        $saferpay_capture = [
            "RequestHeader" => [
                "SpecVersion" => "1.16",
                "CustomerId" => $this->_customer_id,
                "RequestId" => $request_id,
                "RetryIndicator" => 0
            ],
            "TransactionReference" => [
                "TransactionId" => $transaction_id
            ]
        ];

        $capture_response = $this->getTransactionResponsive('Transaction/Capture', $saferpay_capture);

        return $capture_response;
    }

    private function getTransactionResponsive(string $saferpay_current_url, array $data): array
    {
        $response = Http::post($this->_url . $saferpay_current_url, json_encode($data), $this->getSaferpayHeaders());
        $response = json_decode($response, true);

        return $response;
    }

    /**
     * @param mix $total value amount for saferpay paying 
     * @return string this total
     */
    public function getCurentSaferpayValue($total)
    {
        return (string) fn_format_rate_value($total, '', 2, '', '', '');
    }


    /**
     * @return array Saferpay important Headers
     */
    protected function getSaferpayHeaders()
    {
        return array(
            'headers' => array(
                'Content-Type: application/json; charset=utf-8',
                'Accept: application/json'
            ),
            'basic_auth' => array(
                'username' => $this->_username,
                'password' => $this->_password,
            )
        );
    }

    /**
     * @return string Saferpay error_name || error_code 
     */
    public function getErrorName()
    {
        return $this->_error_name;
    }

    /**
     * @return string Saferpay error_message
     */
    public function getErrorMessage()
    {
        return $this->_error_message;
    }
}
