<?php


if (!defined('BOOTSTRAP')) {
    die('Access denied');
}



function fn_altteam_saferpay_install()
{
    fn_altteam_saferpay_uninstall();

    $_data = array(
        'processor' => 'Saferpay',
        'processor_script' => 'saferpay.php',
        'processor_template' => 'views/orders/components/payments/cc_outside.tpl',
        'admin_template' => 'saferpay.tpl',
        'callback' => 'Y',
        'type' => 'P',
        'addon' => 'altteam_saferpay'
    );

    db_query("INSERT INTO ?:payment_processors ?e", $_data);
}

function fn_altteam_saferpay_uninstall()
{
    db_query("DELETE FROM ?:payment_processors WHERE processor_script = ?s", "saferpay.php");
}

function fn_altteam_saferpay_payment_methods()
{
    return array("ALIPAY", "AMEX", "BANCONTACT", "BONUS", "DINERS", "DIRECTDEBIT", "EPRZELEWY", "EPS", "GIROPAY", "IDEAL", "INVOICE", "JCB", "MAESTRO", "MASTERCARD", "MYONE", "PAYPAL", "PAYDIREKT", "POSTCARD", "POSTFINANCE", "SAFERPAYTEST", "SOFORT", "TWINT", "UNIONPAY", "VISA", "VPAY");
}

function fn_altteam_saferpay_langs()
{
    return array("de", "de-ch", "en", "fr", "da", "cs", "es", "et", "hr", "it", "hu", "lv", "lt", "nl", "nn", "pl", "pt", "ru", "ro", "sk", "sl", "fi", "sv", "tr", "el", "ja", "zh");
}
