<?php

use Tygh\Payments\Processors\Saferpay;

if (defined('PAYMENT_NOTIFICATION')) {

    $order_id =  0;

    if (!empty($_REQUEST['ordernumber'])) {
        $order_id = $_REQUEST['ordernumber'];
    }

    $order_info = fn_get_order_info($order_id);


    if ($mode == 'return') {

        if (empty($processor_data) && !empty($order_info)) {
            $processor_data = fn_get_processor_data($order_info['payment_id']);
        }

        $saferpay = new Saferpay($processor_data);

        if (!empty($order_info['payment_info']['token'])) {
            $assert_response = $saferpay->saferpayPaymentAssert($order_id, $order_info['payment_info']['token']);

            if ($assert_response['Transaction']['Status'] == 'AUTHORIZED') {
                $capture_response = $saferpay->saferpayPaymentCapture($assert_response['ResponseHeader']['RequestId'], $assert_response['Transaction']['Id']);
                $pp_response = [
                    'order_status' => 'P'
                ];
            } elseif ($assert_response['Transaction']['Status'] == 'CAPTURED') {
                $pp_response = [
                    'order_status' => 'P'
                ];
            } elseif ($assert_response['Transaction']['Status'] == 'PENDING') {
                $pp_response = [
                    'order_status' => 'O'
                ];
            } else {
                $pp_response = [
                    'order_status' => 'D'
                ];
            }
            fn_finish_payment($order_id, $pp_response);
        }


        fn_order_placement_routines('route', $order_id, true);
    }

    if ($mode == 'error') {
        $pp_response = array(
            'order_status' => 'F',
        );

        fn_update_order_payment_info($order_id, $pp_response);
        fn_order_placement_routines('route', $order_id);
    }

    if ($mode == 'abort') {

        $pp_response = array(
            'order_status' => 'D',
        );

        fn_update_order_payment_info($order_id, $pp_response);
        fn_order_placement_routines('route', $order_id);
    }
    exit;
} else {
    $saferpay = new Saferpay($processor_data);
    $response = $saferpay->saferpayPaymentInitialize($order_info);

    if (empty($saferpay->getErrorName())) {

        $pp_response = array(
            'transaction_id' => $response['order_id']
        );

        fn_update_order_payment_info($order_id, $pp_response);
        fn_create_payment_form($response['RedirectUrl'], array(), 'Saferpay', true, 'POST');
    } else {
        $pp_response['order_status'] = 'F';
        $pp_response['reason_text'] = $saferpay->getErrorMessage();

        fn_finish_payment($order_id, $pp_response);
        fn_order_placement_routines('route', $order_id, false);
    }
}
