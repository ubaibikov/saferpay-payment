<div class="control-group">
    <label class="control-label" for="saferpay_username">{__("addons.saferpay.username")}:</label>
    <div class="controls">
        <input type="text" name="payment_data[processor_params][username]" id="saferpay_username" value="{$processor_params.username}"  size="60">
    </div>
</div>

<div class="control-group">
    <label class="control-label" for="saferpay_password">{__("addons.saferpay.password")}:</label>
    <div class="controls">
        <input type="text" name="payment_data[processor_params][password]" id="saferpay_password" value="{$processor_params.password}"  size="60">
    </div>
</div>

<div class="control-group">
    <label class="control-label" for="saferpay_wallets">{__("addons.saferpay.wallets")}:</label>
    <div class="controls">
        <select name="payment_data[processor_params][wallets][]" multiple>
            {foreach fn_altteam_saferpay_payment_methods() as $wallets}
                <option value="{$wallets}"{if $wallets|in_array:$processor_params.wallets}selected="selected"{/if}>{$wallets}</option>
            {/foreach}
        </select>
    </div>
</div>

<div class="control-group">
    <label class="control-label" for="saferpay_payment_methods">{__("addons.saferpay.payment_methods")}:</label>
    <div class="controls">
        <select name="payment_data[processor_params][payment_methods][]" multiple>
            {foreach fn_altteam_saferpay_payment_methods() as $payment_methods}
                <option value="{$payment_methods}"{if $payment_methods|in_array:$processor_params.payment_methods}selected="selected"{/if}>{$payment_methods}</option>
            {/foreach}
        </select>
    </div>
</div>

<div class="control-group">
    <label class="control-label" for="saferpay_lang">{__("addons.saferpay.lang")}:</label>
    <div class="controls">
        <select name="payment_data[processor_params][lang]">
            {foreach fn_altteam_saferpay_langs() as $lang}
                <option value="{$lang}"{if $processor_params.lang == $lang}selected="selected"{/if}>{$lang|strtoupper}</option>
            {/foreach}
        </select>
    </div>
</div>

<div class="control-group">
    <label class="control-label" for="saferpay_customer_id">{__("addons.saferpay.customer_id")}:</label>
    <div class="controls">
        <input type="text" name="payment_data[processor_params][customer_id]" id="saferpay_customer_id" value="{$processor_params.customer_id}"  size="60">
    </div>
</div>

<div class="control-group">
    <label class="control-label" for="saferpay_login">{__("addons.saferpay.terminal")}:</label>
    <div class="controls">
        <input type="text" name="payment_data[processor_params][terminal]" id="saferpay_terminal" value="{$processor_params.terminal}"  size="60">
    </div>
</div>


<div class="control-group">
    <label class="control-label" for="mode">{__("test_live_mode")}:</label>
    <div class="controls">
        <select name="payment_data[processor_params][mode]" id="mode">
            <option value="test" {if $processor_params.mode == "test"}selected="selected"{/if}>{__("test")}</option>
            <option value="live" {if $processor_params.mode == "live"}selected="selected"{/if}>{__("live")}</option>
        </select>
    </div>
</div>


<div class="control-group">
    <label class="control-label" for="currency_{$payment_id}">{__("currency")}:</label>
    <div class="controls">
        <select name="payment_data[processor_params][currency]" id="currency_{$payment_id}">
            <option value="EUR"{if $processor_params.currency == "EUR"} selected="selected"{/if}>{__("currency_code_eur")}</option>
            <option value="USD"{if $processor_params.currency == "USD"} selected="selected"{/if}>{__("currency_code_usd")}</option>
        </select>
    </div>
</div>

